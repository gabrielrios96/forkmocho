USE [AdministradorDeProyectos1]
GO

/****** Object:  Table [dbo].[Direcciones]    Script Date: 09/09/2019 18:56:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Direcciones](
	[IDDireccion] [int] IDENTITY(1,1) NOT NULL,
	[Domicilio] [varchar](128) NOT NULL,
	[Localidad] [varchar](128) NOT NULL,
	[Provincia] [varchar](128) NOT NULL,
	[Pais] [varchar](128) NOT NULL,
	[IDUnidadDeNegocio] [int] NOT NULL
 CONSTRAINT [PK_Direcciones] PRIMARY KEY CLUSTERED 
(
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


