﻿CREATE TABLE [dbo].[Equipo]
(
	[IdEquipo] INT NOT NULL identity(1,1) PRIMARY KEY, 
    [Nombre] VARCHAR(75) NOT NULL, 
    [Pais] VARCHAR(75) NOT NULL, 
    [HusoHorario] INT NOT NULL,
    [CantidadProgramadores] INT NOT NULL,

)
