﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.Repositorios
{
    public class SkillsRepository : ISkillRepository
    {
        private EquipoRepository _contexto = new EquipoRepository();

        public IUnitOfWork UnitOfWork => _contexto;

        public Skills Add(Skills skill)
        {
            _contexto.Skills.Add(skill);
            return skill;
        }

        public void Delete(Skills skill)
        {
            _contexto.Skills.Remove(skill);
        }

        public Skills GetSkill(int id)
        {
            var skills = _contexto.Skills.Find(id);
            return skills;
        }

        public List<Skills> ListSkill()
        {
            return _contexto.Skills.ToList();
        }

        public void Update(Skills skill)
        {
            _contexto.Entry(skill).State = EntityState.Modified;
        }
    }

    
}
