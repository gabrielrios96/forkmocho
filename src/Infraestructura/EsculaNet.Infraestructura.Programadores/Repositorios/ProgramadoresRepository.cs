﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.Repositorios
{
    public class ProgramadoresRepository : IProgramadorRepository
    {
        private EquipoContext _contexto = new EquipoContext();

        public IUnitOfWork UnitOfWork => _contexto;

        public Programador Add(Programador programador)
        {
            _contexto.Programadores.Add(programador);
            return programador;
        }

        public void Delete(Programador programador)
        {
            _contexto.Programadores.Remove(programador);
        }

        public Programador GetProgramador(int id)
        {
            var programador = _contexto.Programadores.Find(id);
            if (programador != null)
            {
                _contexto.Entry(programador);

                _contexto.Entry(programador)
                    .Collection(i => i.Skills).Load();
            }

            return programador;
        }

        public List<Programador> ListProgramador()
        {
            return _contexto.Programadores.ToList();
        }

        public void Update(Programador programador)
        {
            _contexto.Entry(programador).State = EntityState.Modified;
        }
    }
}
