﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.Repositorios
{
    public class EquiposRepository : IEquipoRepository
    {
        private EquipoContext _contexto = new EquipoContext();

        public IUnitOfWork UnitOfWork => _contexto;
        public Equipo Add(Equipo equipo)
        {
            _contexto.Equipos.Add(equipo);
            return equipo;
        }

        public void Delete(Equipo equipo)
        {
            _contexto.Equipos.Remove(equipo);
        }

        public void DeleteProgramador(Programador programador)
        {
            if (programador != null)
            {
                _contexto.Programadores.Remove(programador);

            }
           
        }

        public Equipo GetEquipo(int id)
        {
            var equipo = _contexto.Equipos.Find(id);
            if (equipo != null)
            {
                _contexto.Entry(equipo)
                    .Collection(s => s.Skills).Load();
                _contexto.Entry(equipo)
                    .Collection(p => p.Programadores).Load();
            }
            return equipo;
        }

        public List<Equipo> ListEquipo()
        {
            return _contexto.Equipos.ToList();
        }

        public void Update(Equipo equipo)
        {
            _contexto.Entry(equipo).State = EntityState.Modified;
        }
    }
}
