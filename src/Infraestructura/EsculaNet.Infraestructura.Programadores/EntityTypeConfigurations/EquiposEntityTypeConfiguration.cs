﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.EntityTypeConfigurations
{
    public class EquiposEntityTypeConfiguration : EntityTypeConfiguration <Equipo>
    {
        public EquiposEntityTypeConfiguration()
        {
            this.ToTable("Equipo");
            this.HasKey<int>(e => e.ID);
            this.Property(e => e.ID)
                .HasColumnName("IdEquipo");
            this.Property(e => e.Nombre)
                .IsRequired();
            this.Property(e => e.Pais)
                .IsRequired();
            this.Property(e => e.HusoHorario)
                .IsRequired();
            this.Property(e => e.CantidadProgramadores)
                .IsRequired();
            this.Property(e => e.HorasDisponibleProgramadores)
                .IsRequired();
            this.HasMany<Programador>(e => e.Programadores)
                .WithRequired(p => p.Equipo)
                .HasForeignKey<int>(p => p.Equipo_ID);
            this.HasMany<Skills>(e => e.Skills)
                .WithMany(s => s.Equipo)
                .Map(it => {
                    it.MapLeftKey("Equipo_ID");
                    it.MapRightKey("Skill_ID");
                    it.ToTable("EquipoSkill");
                });
        }
    }
}
