﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.EntityTypeConfigurations
{
    public class SkillsEntityTypeConfiguration : EntityTypeConfiguration <Skills>
    {
        public SkillsEntityTypeConfiguration()
        {
            this.ToTable("Skill");
            this.HasKey<int>(s => s.ID);
            this.Property(s => s.ID)
                .HasColumnName("IdSkill");
            this.Property(s => s.Descripcion)
                .IsRequired();
            this.Property(s => s.Grados)
                .IsRequired();

        }
    }
}
