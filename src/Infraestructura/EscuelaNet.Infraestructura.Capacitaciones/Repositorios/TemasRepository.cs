﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Capacitaciones.Repositorios
{
    public class TemasRepository : ITemaRepository
    {
        public TemasRepository()
        {

        }
        private CapacitacionContext _contexto = new CapacitacionContext(); 
        public IUnitOfWork UnitOfWork => _contexto;

        public Tema Add(Tema tema)
        {
            _contexto.Temas.Add(tema);
            return tema;
        }

        public void Delete(Tema tema)
        {
            _contexto.Temas.Remove(tema);
        }

        public Tema GetTema(int id)
        {
            var tema = _contexto.Temas.Find(id);
            if(tema != null)
            {
                _contexto.Entry(tema);
            }
            return tema;
        }

        public List<Tema> ListTemas()
        {
            return _contexto.Temas.ToList();
        }

        public void Update(Tema tema)
        {
            _contexto.Entry(tema).State = EntityState.Modified;

        }
    }
}
