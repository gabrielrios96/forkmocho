﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations
{
    public class EtapaEntityTypeConfiguration : EntityTypeConfiguration<Etapa>
    {
        public EtapaEntityTypeConfiguration()
        {
            this.ToTable("Etapas");
            this.HasKey<int>(e => e.ID);
            this.Property(e => e.ID).HasColumnName("IDEtapa");
            this.Property(e => e.Nombre).IsRequired();
            this.Property(e => e.Duracion).IsRequired();
            
        }
    }
}
