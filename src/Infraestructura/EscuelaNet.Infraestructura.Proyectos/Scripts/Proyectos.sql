USE [Proyectos]
GO

/****** Object:  Table [dbo].[Proyectos]    Script Date: 11/09/2019 10:45:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Proyectos](
	[IDProyecto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](120) NOT NULL,
	[NombreResponsable] [nchar](120) NOT NULL,
	[EmailResponsable] [nchar](120) NOT NULL,
	[TelefonoResponsable] [nchar](120) NOT NULL,
	[IDEtapa] [int] NOT NULL,
 CONSTRAINT [PK_Proyectos] PRIMARY KEY CLUSTERED 
(
	[IDProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO