﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var entorno = ConfigurationManager.AppSettings["Entorno"];
            switch(entorno)
            {
                case "Produccion":
                    var connectionString = 
                        ConfigurationManager
                            .ConnectionStrings["CapacitacionContext"].ToString();
                    builder.RegisterType<CapacitacionContext>()
                        .InstancePerRequest();
                    builder.Register(c => new TemasQuery(
                            connectionString))
                        .As<ITemasQuery>()
                        .InstancePerLifetimeScope();
                    builder.RegisterType<TemasRepository>()
                        .As<ITemaRepository>()
                        .InstancePerLifetimeScope();
                    builder.RegisterType<LugaresRepository>()
                        .As<ILugarRepository>()
                        .InstancePerLifetimeScope();
                    builder.RegisterType<InstructoresRepository>()
                       .As<IInstructorRepository>()
                       .InstancePerLifetimeScope();
                    break;
                case "Singleton":
                default:
                    builder.RegisterType<LugaresSingletonRepository>()
                        .As<ILugarRepository>()
                        .InstancePerLifetimeScope();
                    break;

            }

            base.Load(builder);
        }
    }
}