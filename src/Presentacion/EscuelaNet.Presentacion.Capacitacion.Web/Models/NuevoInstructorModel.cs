﻿using EscuelaNet.Dominio.Capacitaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoInstructorModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public DateTime? FechaNacimiento { get; set; }
    }
}