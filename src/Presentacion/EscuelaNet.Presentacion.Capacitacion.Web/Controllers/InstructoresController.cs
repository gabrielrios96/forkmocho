﻿using EscuelaNet.Dominio.Capacitaciones;
//using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class InstructoresController : Controller
    {
        private IInstructorRepository _instructorRepository;
        private ITemaRepository _temaRepository;
        private ITemasQuery _temaQuery;

        public InstructoresController(ITemaRepository temaRepository,
            IInstructorRepository instructorRepository,
            ITemasQuery temaQuery)
        {
            _temaRepository = temaRepository;
            _instructorRepository = instructorRepository;
            _temaQuery = temaQuery;
        }
        // GET: Instructor
        public ActionResult Index()
        {
            var instructores = _instructorRepository.ListInstructores();

            var model = new InstructorIndexModel()
            {
                Titulo = "Instructores",
                Instructores = instructores,

            };
            return View(model);
        }


        // GET: Instructor/New
        public ActionResult New()
        {
            var model = new NuevoInstructorModel()
            {
                FechaNacimiento = null
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult New(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre)
                || string.IsNullOrEmpty(model.Apellido)
                || string.IsNullOrEmpty(model.Dni)
                || !model.FechaNacimiento.HasValue)
            {

                TempData["error"] = "Nada de Espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    _instructorRepository.Add(new Instructor(model.Nombre, model.Apellido,
                        model.Dni, model.FechaNacimiento.Value));

                    _instructorRepository.UnitOfWork.SaveChanges();

                    TempData["success"] = "Instructor Agregado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }

        }

        // GET: Instructor/Edit/5
        public ActionResult Edit(int id)
        {
            var instructor = _instructorRepository.GetInstructor(id);
            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre)
                || string.IsNullOrEmpty(model.Apellido)
                || string.IsNullOrEmpty(model.Dni)
                || !model.FechaNacimiento.HasValue)
            {

                TempData["error"] = "Nada de espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    var instructor = _instructorRepository.GetInstructor(model.Id);
                    instructor.Nombre = model.Nombre;
                    instructor.Apellido = model.Apellido;
                    instructor.Dni = model.Dni;
                    instructor.FechaNacimiento = model.FechaNacimiento.Value;

                    _instructorRepository.Update(instructor);
                    _instructorRepository.UnitOfWork.SaveChanges();

                    TempData["success"] = "Instructor Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
        }



        // GET: Instructor/Delete/5
        public ActionResult Delete(int id)
        {
            var instructor = _instructorRepository.GetInstructor(id);

            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoInstructorModel model)
        {
            try
            {
                var instructor = _instructorRepository.GetInstructor(model.Id);
                _instructorRepository.Delete(instructor);
                _instructorRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult Temas(int id)
        {
            var instructor = _instructorRepository.GetInstructor(id);
            var model = new InstructorTemaModel()
            {
                Instructor = instructor,
                Temas = instructor.Temas.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteTema(int id, int instructor)
        {
            var instructorBuscado = _instructorRepository.GetInstructor(instructor);
            var temabuscado = instructorBuscado.Temas.First(t => t.ID == id);
            var model = new NuevoInstructorTemaModel()
            {
                NombreTema = temabuscado.Nombre + " " + temabuscado.Nivel,
                IDInstructor = instructor,
                NombreApellido = instructorBuscado.Nombre + " " + instructorBuscado.Apellido,
                IDTema = id,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult DeleteTema(NuevoInstructorTemaModel model)
        {
            try
            {
                var instructorBuscado = _instructorRepository.GetInstructor(model.IDInstructor);
                var temabuscado = instructorBuscado.Temas.First(t => t.ID == model.IDTema);
                instructorBuscado.pullTema(temabuscado);
                _instructorRepository.Update(instructorBuscado);
                _instructorRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewTema(int id)
        {
            var instructor = _instructorRepository.GetInstructor(id);
            var temas = _temaQuery.ListTemas();
            var model = new NuevoInstructorTemaModel()
            {
                IDInstructor = id,
                NombreApellido = instructor.Nombre + " " + instructor.Apellido,
                Temas = temas
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewTema(NuevoInstructorTemaModel model)
        {



            try
            {
                var instructorBuscado = _instructorRepository.GetInstructor(model.IDInstructor);
                var temabuscado = _temaRepository.GetTema(model.IDTema);

                instructorBuscado.pushTema(temabuscado);
                _instructorRepository.Update(instructorBuscado);
                _instructorRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }

}
