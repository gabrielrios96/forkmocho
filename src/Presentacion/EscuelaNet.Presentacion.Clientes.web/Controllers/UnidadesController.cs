﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class UnidadesController : Controller
    {
        private IClienteRepository Repositorio = new ClientesRepository();
        private ISolicitudRepository RepositorioSolicitudes = new SolicitudesRepository();

        public ActionResult Index(int id)
        {
            var cliente = Repositorio.GetCliente(id);           
            if (cliente.Unidades==null)
            {
                TempData["error"] = "Cliente sin unidades";
                return RedirectToAction("../Clientes/Index");
            }
            var model = new UnidadesIndexModel()
            {
                Titulo = "Unidades del Cliente '" + cliente.RazonSocial+"'",
                Unidades = cliente.Unidades.ToList(),
                IdCliente = id
            };

            return View(model);
        }

        public ActionResult New(int id)
        {
            var cliente = Repositorio.GetCliente(id);
            var model = new NuevaUnidadModel()
            {
                Titulo = "Nueva unidad para el Cliente '"+cliente.RazonSocial+"'",
                IdCliente = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial)
                || !string.IsNullOrEmpty(model.ResponsableDeUnidad)
                || !string.IsNullOrEmpty(model.Cuit)
                || !string.IsNullOrEmpty(model.EmailResponsable)
                || !string.IsNullOrEmpty(model.TelefonoResponsable))
            {
                try
                {                    
                    var razonSocial = model.RazonSocial;
                    var responsable = model.ResponsableDeUnidad;
                    var cuit = model.Cuit;
                    var email = model.EmailResponsable;
                    var telefono = model.TelefonoResponsable;

                    var cliente = Repositorio.GetCliente(model.IdCliente);
                    
                    var unidad = new UnidadDeNegocio(razonSocial, responsable, cuit, email, telefono);
                    cliente.AgregarUnidad(unidad);
                    Repositorio.Update(cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad creada";
                    return RedirectToAction("Index/"+model.IdCliente);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        
        public ActionResult Edit(int id)
        {

            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var unidad = Repositorio.GetUnidadDeNegocio(id);              

                var model = new NuevaUnidadModel()
                {
                    Titulo="Editar la Unidad '"+unidad.RazonSocial+"' del Cliente '"+ unidad.Cliente.RazonSocial+"'",
                    IdCliente = unidad.Cliente.ID,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                                       
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial)
                || !string.IsNullOrEmpty(model.ResponsableDeUnidad)
                || !string.IsNullOrEmpty(model.Cuit)
                || !string.IsNullOrEmpty(model.EmailResponsable)
                || !string.IsNullOrEmpty(model.TelefonoResponsable))
            {
                try
                {
                    var cliente = Repositorio.GetCliente(model.IdCliente);                    
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().RazonSocial = model.RazonSocial;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().ResponsableDeUnidad = model.ResponsableDeUnidad;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().Cuit = model.Cuit;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().EmailResponsable = model.EmailResponsable;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().TelefonoResponsable = model.TelefonoResponsable;

                    Repositorio.Update(cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad editada";
                    return RedirectToAction("Index/"+model.IdCliente);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id < 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var unidad = Repositorio.GetUnidadDeNegocio(id);                
                var model = new NuevaUnidadModel()
                {
                    Titulo = "Borrar la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.Cliente.RazonSocial + "'",
                    IdCliente = unidad.Cliente.ID,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaUnidadModel model)
        {

            try
            {
                var unidad = Repositorio.GetUnidadDeNegocio(model.IdUnidad);
                Repositorio.DeleteUnidadDeNegocio(unidad);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Unidad borrada";
                return RedirectToAction("../Unidades/Index/"+model.IdCliente);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

        public ActionResult Solicitudes(int id)
        {

            var unidad = Repositorio.GetUnidadDeNegocio(id);
            var model = new UnidadSolicitudModel()
            {
                Unidad = unidad,
                Solicitudes = unidad.Solicitudes.ToList()
            };

            return View(model);

        }

        public ActionResult UnlinkSolicitud(int id, int unidad)
        {
            var unidadBuscada = Repositorio.GetUnidadDeNegocio(unidad);
            var solicitudBuscada = unidadBuscada.Solicitudes.First(s => s.ID == id);
            var model = new NuevaUnidadSolicitudModel()
            {
                IDSolicitud = id,
                IDUnidad = unidad,
                TituloSolicitud = solicitudBuscada.Titulo,
                RazonSocialUnidad = unidadBuscada.RazonSocial
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult UnlinkSolicitud(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var unidadBuscada = Repositorio.GetUnidadDeNegocio(model.IDUnidad);
                var solicitudBuscada = unidadBuscada.Solicitudes.First(s => s.ID == model.IDSolicitud);

                Repositorio.RemoveSolicitud(unidadBuscada, solicitudBuscada);

                Repositorio.Update(unidadBuscada.Cliente);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Solicitud desvinculada";
                return RedirectToAction("../Unidades/Solicitudes/"+model.IDUnidad);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult LinkSolicitud(int id)
        {
            var unidad = Repositorio.GetUnidadDeNegocio(id);
            var solicitudes = RepositorioSolicitudes.ListSolicitud();

            var model = new NuevaUnidadSolicitudModel()
            {
                IDUnidad = id,
                RazonSocialUnidad = unidad.RazonSocial,
                Solicitudes = solicitudes
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LinkSolicitud(NuevaUnidadSolicitudModel model)
        {
                       
            try
            {
                var unidadBuscada = Repositorio.GetUnidadDeNegocio(model.IDUnidad);

                var solicitudBuscada = RepositorioSolicitudes.GetSolicitud(model.IDSolicitud);

                var bandera = true;
                var bandera2 = true;

                foreach (var unidadLista in solicitudBuscada.UnidadesDeNegocio)
                {
                    if (unidadLista.IDCliente != unidadBuscada.Cliente.ID )
                    {
                        bandera = false;
                    }
                    if (unidadLista.ID == model.IDUnidad)
                    {
                        bandera2 = false;
                    }
                }
                if (bandera && bandera2)
                {

                    Repositorio.AddSolicitud(unidadBuscada, solicitudBuscada);

                    Repositorio.Update(unidadBuscada.Cliente);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud vinculada";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);

                }
                else if(bandera2)
                {
                    TempData["error"] = "Solicitud vinculada a una unidad de otro cliente";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
                }
                else
                {
                    TempData["error"] = "Solicitud ya vinculada a esta Unidad";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
            }

        }


    }
}