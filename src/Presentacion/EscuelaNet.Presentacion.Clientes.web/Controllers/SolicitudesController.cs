﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class SolicitudesController : Controller
    {
        private ISolicitudRepository Repositorio = new SolicitudesRepository();
        private IClienteRepository RepositorioCliente = new ClientesRepository();

        public ActionResult Index()
        {
                var solicitudes = Repositorio.ListSolicitud();                      
                var model = new SolicitudesIndexModel()
                {
                    Titulo = "Solicitudes",
                    Solicitudes = solicitudes,                    
                };

                return View(model);                          
        }

        public ActionResult New()
        {                
            var model = new NuevaSolicitudModel()
            {
                Title = "Nueva Solicitud ",
            };
            return View(model);                        
        }

        [HttpPost]
        public ActionResult New(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = new Solicitud(model.Titulo, model.Descripcion);
                    Repositorio.Add(solicitud);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud creada";
                    
                    return RedirectToAction("Index");       
                        
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var solicitud = Repositorio.GetSolicitud(id);
              
                var model = new NuevaSolicitudModel()
                {
                    Title = "Editar la solicitud " + solicitud.Titulo,
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = Repositorio.GetSolicitud(model.IdSolicitud);
                    solicitud.Titulo = model.Titulo;
                    solicitud.Descripcion = model.Descripcion;
                    solicitud.CambiarEstado(model.Estado);

                    Repositorio.Update(solicitud);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Solicitud editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var solicitud = Repositorio.GetSolicitud(id);

                var model = new NuevaSolicitudModel()
                {
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }               
        }

        [HttpPost]
        public ActionResult Delete(NuevaSolicitudModel model)
        {
            try
            {
                var solicitud = Repositorio.GetSolicitud(model.IdSolicitud);
                Repositorio.Delete(solicitud);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Solicitud borrada";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Unidades(int id)
        {

            var solicitud = Repositorio.GetSolicitud(id);
            var model = new UnidadSolicitudModel()
            {
                Solicitud = solicitud,
                Unidades = solicitud.UnidadesDeNegocio.ToList()
            };

            return View(model);

        }

        public ActionResult UnlinkUnidad(int id, int solicitud)
        {
            var solicitudBuscada = Repositorio.GetSolicitud(solicitud);
            var unidadBuscada = solicitudBuscada.UnidadesDeNegocio.First(u => u.ID == id);
            var model = new NuevaUnidadSolicitudModel()
            {
                IDSolicitud = solicitud,
                IDUnidad = id,
                TituloSolicitud = solicitudBuscada.Titulo,
                RazonSocialUnidad = unidadBuscada.RazonSocial
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult UnlinkUnidad(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var solicitudBuscada = Repositorio.GetSolicitud(model.IDSolicitud);
                var unidadBuscada = RepositorioCliente.GetUnidadDeNegocio(model.IDUnidad);
                Repositorio.removeUnidad(solicitudBuscada, unidadBuscada);

                Repositorio.Update(solicitudBuscada);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Solicitud desvinculada";
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult LinkUnidad(int id)
        {
            var solicitud = Repositorio.GetSolicitud(id);
            var clientes = RepositorioCliente.ListCliente();
            var unidades = new List<UnidadDeNegocio>();

            foreach (var cliente in clientes)
            {
                foreach (var unidadEnCliente in cliente.Unidades)
                {
                    unidades.Add(unidadEnCliente);
                }               
            }
            var model = new NuevaUnidadSolicitudModel()
            {
                IDSolicitud = id,
                TituloSolicitud = solicitud.Titulo,
                Unidades = unidades
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LinkUnidad(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var solicitudBuscada = Repositorio.GetSolicitud(model.IDSolicitud);
                var unidadBuscada = RepositorioCliente.GetUnidadDeNegocio(model.IDUnidad);

                var bandera = true;
                var bandera2 = true;

                foreach (var unidadLista in solicitudBuscada.UnidadesDeNegocio)
                {                    
                    if (unidadLista.IDCliente != unidadBuscada.Cliente.ID )
                    {
                        bandera = false;
                    }
                    if (unidadLista.ID == model.IDUnidad)
                    {
                        bandera2 = false;
                    }
                }
                if (bandera && bandera2)
                {
                    RepositorioCliente.AddSolicitud(unidadBuscada, solicitudBuscada);
                    RepositorioCliente.Update(unidadBuscada.Cliente);
                    RepositorioCliente.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud vinculada";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }
                else if (bandera2)
                {
                    TempData["error"] = "Solicitud vinculada a una unidad de otro cliente";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }
                else
                {
                    TempData["error"] = "Solicitud ya vinculada a esta unidad";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }                
            }
            catch (Exception ex)
            {                
                TempData["error"] = ex.Message;
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }

        }


    }
}