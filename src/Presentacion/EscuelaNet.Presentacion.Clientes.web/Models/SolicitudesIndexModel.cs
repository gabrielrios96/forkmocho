﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class SolicitudesIndexModel
    {
        public string Titulo { get; set; }      

        public List<Solicitud> Solicitudes { get; set; }
    }
}