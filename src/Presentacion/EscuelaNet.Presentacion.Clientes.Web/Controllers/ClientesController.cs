﻿//using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class ClientesController : Controller
    {
        private IClienteRepository Repositorio = new ClientesRepository();
        // GET: Clientes
        public ActionResult Index()
        {
            var clientes = Repositorio.ListCliente();

            var model = new ClientesIndexModel()
            {
                Titulo = "Index clientes",
                Clientes = clientes
            };

            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoClienteModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    Repositorio.Add(new Cliente(
                            model.RazonSocial, model.Email, model.Categoria
                        ));
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Cliente creado";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else { 
            var cliente = Repositorio.GetCliente(id);

            var model = new NuevoClienteModel()
            {
                Id = id,
                RazonSocial = cliente.RazonSocial,
                Email = cliente.Email,
                Categoria = cliente.Categoria
            };

            return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    var cliente = Repositorio.GetCliente(model.Id);
                    cliente.RazonSocial = model.RazonSocial;
                    cliente.Email = model.Email;
                    cliente.Categoria = model.Categoria;

                    Repositorio.Update(cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Cliente editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else { 
                var cliente = Repositorio.GetCliente(id);
            
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoClienteModel model)
        {

            try
            {
                var cliente = Repositorio.GetCliente(model.Id);
                Repositorio.Delete(cliente);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Cliente borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
            
        }

    }
}