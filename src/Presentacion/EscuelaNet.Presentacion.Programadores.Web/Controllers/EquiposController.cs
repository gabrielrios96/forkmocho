﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class EquiposController : Controller
    {
        private IEquipoRepository Repositorio = new EquiposRepository();
        private ISkillRepository RepositorioSKill = new SkillsRepository();
        private IProgramadorRepository RepositorioProgramador = new ProgramadoresRepository();
        // GET: Equipos
        public ActionResult Index()
        {
            var equipos = Repositorio.ListEquipo();

            var model = new EquiposIndexModel()
            {
                Titulo = "Prueba de Equipos",
                Equipos = equipos
            };
            return View(model);
        }
        public ActionResult New()
        {
            var model = new NuevoEquipoModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult New(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var equipo = new Equipo(model.Nombre, model.Pais, model.HusoHorario);
                    Repositorio.Add(equipo);

                    Repositorio.UnitOfWork.SaveChanges();
                    
                    TempData["success"] = "Equipo Creado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Equipo no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var equipo = Repositorio.GetEquipo(id);
                var model = new NuevoEquipoModel()
                {
                    Nombre = equipo.Nombre,
                    Pais = equipo.Pais,
                    HusoHorario = equipo.HusoHorario,
                    IdEquipo = id
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var equipo = Repositorio.GetEquipo(model.IdEquipo);
                    equipo.Nombre = model.Nombre;
                    equipo.Pais = model.Pais;
                    equipo.HusoHorario = model.HusoHorario;

                    Repositorio.Update(equipo);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Equipo Editado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Equipo no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var equipo = Repositorio.GetEquipo(id);
                var model = new NuevoEquipoModel()
                {
                    Nombre = equipo.Nombre,
                    Pais = equipo.Pais,
                    HusoHorario = equipo.HusoHorario,
                    IdEquipo = id
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoEquipoModel model)
        {

            try
            {
                var equipo = Repositorio.GetEquipo(model.IdEquipo);
                Repositorio.Delete(equipo);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Equipo Borrado Correctamente";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Skills(int id)
        {
            var equipo = Repositorio.GetEquipo(id);
            var model = new EquipoSkillModel()
            {
                Equipo = equipo,
                Skills = equipo.Skills.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteSkills(int id, int idEquipo)
        {
            var equipoBuscado = Repositorio.GetEquipo(idEquipo);
            var skillBuscado = equipoBuscado.Skills.First(s => s.ID == id);

            var model = new NuevoEquipoSkillModel()
            {
                DescripcionSkill = skillBuscado.Descripcion + " " + skillBuscado.Grados,
                IdEquipo = idEquipo,
                NombreEquipo = equipoBuscado.Nombre,
                IdSkill = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSkills(NuevoEquipoSkillModel model)
        {
            try
            {
                var equipoBuscado = Repositorio.GetEquipo(model.IdEquipo);
                var skillBuscado = equipoBuscado.Skills.First(s => s.ID == model.IdSkill);
                equipoBuscado.RemoveSkill(skillBuscado);

                Repositorio.Update(equipoBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewSkill(int id)
        {
            var equipo = Repositorio.GetEquipo(id);
            var skill = RepositorioSKill.ListSkills();

            var model = new NuevoEquipoSkillModel()
            {
                IdEquipo = id,
                NombreEquipo = equipo.Nombre,
                Skills = skill
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewSkill(NuevoEquipoSkillModel model)
        {
            try
            {
                var equipoBuscado = Repositorio.GetEquipo(model.IdEquipo);
                var skillBuscado = RepositorioSKill.GetSkill(model.IdSkill);

                equipoBuscado.PushSkill(skillBuscado);

                Repositorio.Update(equipoBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Skill Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }
        public ActionResult Programadores(int id)
        {
            var equipo = Repositorio.GetEquipo(id);
            var model = new EquipoProgramadorModel()
            {
                Equipo = equipo,
                Programadores = equipo.Programadores.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteProgramador(int id, int idEquipo)
        {
            var equipoBuscado = Repositorio.GetEquipo(idEquipo);
            var programadorBuscado = equipoBuscado.Programadores.First(p => p.ID == id);
            var model = new NuevoEquipoProgramadorModel()
            {
                NombreProgramador = programadorBuscado.Nombre + " " + programadorBuscado.Apellido,
                IdEquipo = idEquipo,
                NombreEquipo = equipoBuscado.Nombre,
                IdProgramador = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteProgramador(NuevoEquipoProgramadorModel model)
        {
            try
            {
                var equipoBuscado = Repositorio.GetEquipo(model.IdEquipo);
                var programadorBuscado = equipoBuscado.Programadores.First(s => s.ID == model.IdProgramador);
                
                //equipoBuscado.pullProgramador(programadorBuscado);
                var programador = RepositorioProgramador.GetProgramador(programadorBuscado.ID);
                equipoBuscado.updateProgramador();
                equipoBuscado.CantidadHorasDisponibleProgramadores();

                RepositorioProgramador.Delete(programador);
                RepositorioProgramador.UnitOfWork.SaveChanges();

                Repositorio.Update(equipoBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Programador borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewProgramador(int id)
        {
            var equipo = Repositorio.GetEquipo(id);
            var programador = RepositorioProgramador.ListProgramador();

            var model = new NuevoEquipoProgramadorModel()
            {
                IdEquipo = id,
                NombreEquipo = equipo.Nombre,
                Programadores = programador
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewProgramador(NuevoEquipoProgramadorModel model)
        {
            try
            {
                var equipoBuscado = Repositorio.GetEquipo(model.IdEquipo);
                var programadorBuscado = RepositorioProgramador.GetProgramador(model.IdProgramador);

                equipoBuscado.PushProgramador(programadorBuscado);

                Repositorio.Update(equipoBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Programador Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}