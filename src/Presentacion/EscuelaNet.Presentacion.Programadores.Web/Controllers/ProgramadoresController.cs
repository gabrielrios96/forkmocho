﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class ProgramadoresController : Controller
    {
        private IProgramadorRepository Repositorio = new ProgramadoresRepository();
        private ISkillRepository RepositorioSKill = new SkillsRepository();
        // GET: Programadores
        public ActionResult Index()
        {
            var programador = Repositorio.ListProgramador();

            var model = new ProgramadoresIndexModel()
            {
                Titulo = "Programadores",
                Programadores = programador
            };

             return View(model);
        }

        // GET: Programadores/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Programadores/Create
        public ActionResult New()
        {
            var model = new NuevoProgramadorModel();
            return View(model);
        }

        // POST: Programadores/Create
        [HttpPost]
        public ActionResult New(NuevoProgramadorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var programador = new Programador(model.Nombre, model.Apellido, model.Legajo, model.Dni, model.Rol, model.FechaNacimiento);
                    Repositorio.Add(programador);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Programador Creado";
                    
                    return RedirectToAction("Index");
                }
                catch(Exception e)
                {
                    TempData["Error"] = e.Message;
                    return View(model);
                }
            }
            else
            {
               TempData["error"] = "Texto vacio";
               return View(model);
            }
        }

        // GET: Programadores/Edit/5
        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Programador no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var programador = Repositorio.GetProgramador(id);
                var model = new NuevoProgramadorModel()
                {
                    IdProgramadores = id,
                    Nombre = programador.Nombre,
                    Apellido = programador.Apellido,
                    Legajo = programador.Legajo,
                    Dni = programador.Dni,
                    Rol = programador.Rol,
                    FechaNacimiento = programador.FechaNacimiento,
                    Disponibilidad = programador.Disponibilidad,

                };

                return View(model);
            }
            
        }

        // POST: Programadores/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoProgramadorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var programador = Repositorio.GetProgramador(model.IdProgramadores);
                    programador.Nombre = model.Nombre;
                    programador.Apellido = model.Apellido;
                    programador.Legajo = model.Legajo;
                    programador.Dni = model.Dni;
                    programador.FechaNacimiento = model.FechaNacimiento;
                    programador.Disponibilidad = model.Disponibilidad;

                    Repositorio.Update(programador);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Programador Modificado";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["Error"] = e.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        // GET: Programadores/Delete/5
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Programador no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var programador = Repositorio.GetProgramador(id);
                var model = new NuevoProgramadorModel()
                {
                    IdProgramadores = id,
                    Nombre = programador.Nombre,
                    Apellido = programador.Apellido,
                    Legajo = programador.Legajo,
                    Dni = programador.Dni,
                    Rol = programador.Rol,
                    FechaNacimiento = programador.FechaNacimiento,
                    Disponibilidad = programador.Disponibilidad,
                };

                return View(model);
            }
            
            
        }

        // POST: Programadores/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoProgramadorModel model)
        {
            try
            {
                var programador = Repositorio.GetProgramador(model.IdProgramadores);
                Repositorio.Delete(programador);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Programador Borrado Correctamente";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Skills(int id)
        {
            var programador = Repositorio.GetProgramador(id);
            var model = new ProgramadorSkillModel()
            {
                Programador = programador,
                Skills = programador.Skills.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteSkills(int id, int idProgramador)
        {
            var programdorBuscado = Repositorio.GetProgramador(idProgramador);
            var skillBuscado = programdorBuscado.Skills.First(s => s.ID == id);

            var model = new NuevoProgramadorSkillModel()
            {
                DescripcionSkill = skillBuscado.Descripcion + " " + skillBuscado.Grados,
                IdProgramador = idProgramador,
                NombreProgramador = programdorBuscado.Nombre,
                IdSkill = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSkills(NuevoProgramadorSkillModel model)
        {
            try
            {
                var programadorBuscado = Repositorio.GetProgramador(model.IdProgramador);
                var skillBuscado = programadorBuscado.Skills.First(s => s.ID == model.IdSkill);
                programadorBuscado.RemoveSkill(skillBuscado);

                Repositorio.Update(programadorBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewSkill(int id)
        {
            var programador = Repositorio.GetProgramador(id);
            var skill = RepositorioSKill.ListSkills();

            var model = new NuevoProgramadorSkillModel()
            {
                IdProgramador = id,
                NombreProgramador = programador.Nombre,
                Skills = skill
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewSkill(NuevoProgramadorSkillModel model)
        {
            try
            {
                var programadorBuscado = Repositorio.GetProgramador(model.IdProgramador);
                var skillBuscado = RepositorioSKill.GetSkill(model.IdSkill);

                programadorBuscado.PushConocimiento(skillBuscado);

                Repositorio.Update(programadorBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Skill Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }
    }
}
