﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class NuevoEquipoProgramadorModel
    {
        public int IdProgramador { get; set; }
        public int IdEquipo { get; set; }
        public string NombreEquipo { get; set; }
        public string NombreProgramador { get; set; }
        public List<Programador> Programadores { get; set; }
    }
}