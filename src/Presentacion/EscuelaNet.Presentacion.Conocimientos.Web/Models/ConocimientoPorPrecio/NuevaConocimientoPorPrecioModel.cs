﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevaConocimientoPorPrecioModel
    {
        public DateTime Fecha { set; get; }
        public Nivel Nivel { set; get; }
        public string Moneda { get; set; }
        public Double ValorNominal { get; set; }
    }
}