﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices
{
    public class TemasQuery : ITemasQuery
    {
        private string _connectionString;
        public TemasQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public TemaQueryModel GetTema(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<TemaQueryModel>(
                    @"
                    SELECT t.IDTemas as Id, t.Nombre as Nombre,
                    t.NivelTema as Nivel
                    FROM Temas as t
                    WHERE IDTemas = @id
                    ", new {id = id}
                    ).FirstOrDefault();
            }
        }

        public List<TemaQueryModel> ListTemas()
        {
            using(var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<TemaQueryModel>(
                    @"
                    SELECT t.IDTemas as Id, t.Nombre as Nombre,
                        t.NivelTema as Nivel,
					    it.Total as Total
                    FROM Temas as t
					LEFT JOIN (
					    SELECT IDTema, COUNT(IDTema) as Total
                        FROM InstructorTema 
                        GROUP BY IDTema
					) as it ON t.IDTemas = it.IDTema
                    ORDER BY Total DESC
                    "
                    ).ToList();
            }
        }
    }
}
