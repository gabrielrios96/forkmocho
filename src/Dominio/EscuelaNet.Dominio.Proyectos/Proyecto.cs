﻿using EscuelaNet.Dominio.SeedWoork;
//using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class Proyecto : Entity
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string NombreResponsable { get; set; }
        public long TelefonoResponsable { get; set; }
        public string EmailResponsable { get; set; }

        public IList<Etapa> Etapas { get; set; }
        public DateTime FechaFin;
        public int Duracion;
        public LineaDeProduccion LineaDeProduccion { get; set; }
        public IList<Suceso> Sucesos { get; set; }
        //public int EquipoID { get; set; }
        public IList<Tecnologias> Tecnologias { get; set; } //Demandas Tecnologicas
        //public IList<Programador> Programadores { get; set; }
        private EstadoDeProyecto Estado { get; set; }
        public int IDLinea { get; set; }

        private Proyecto()
        {

        }
        public Proyecto(string Nombre, string Descripcion, string NombreResponsable, long TelefonoResponsable, string EmailResponsable)
        {
            this.Nombre = Nombre;
            this.Descripcion = Descripcion;
            this.NombreResponsable = NombreResponsable;
            this.TelefonoResponsable = TelefonoResponsable;
            this.EmailResponsable = EmailResponsable;
            this.Estado = EstadoDeProyecto.Diseno;

        }

        public void PushEtapa(string nombre, int duracion)
        {
            if (this.Etapas == null)
            {
                this.Etapas = new List<Etapa>();
            }
            this.Etapas.Add(new Etapa(nombre, duracion));
        }
        public void EditarDuracionDeEtapa(int id, int duracion)
        {
            //this.Duracion += duracion;
            this.Etapas[id].Duracion = duracion;
            calcDuracion();
        }
        public void pushTecnologia(string nombre)
        {
            if(this.Tecnologias == null)
            {
                this.Tecnologias = new List<Tecnologias>();
            }
            this.Tecnologias.Add(new Tecnologias(nombre));
        }

        public void CambiarEstado(EstadoDeProyecto estadoDeProyecto)
        {
            if (this.Estado == EstadoDeProyecto.Diseno)
                this.Estado = estadoDeProyecto;
            else if (this.Estado == EstadoDeProyecto.Iniciado && estadoDeProyecto != EstadoDeProyecto.Diseno)
                this.Estado = estadoDeProyecto;
        }
        public string ObtenerEstado()
        {
            return this.Estado.ToString();
        }

        public void calcDuracion()
        {
            if (this.Etapas == null)
                throw new ExcepcionDeProyectos("No hay etapas");
            var duracion = 0;
            foreach (var etapa in Etapas)
            {
                duracion += etapa.Duracion;
            }
            this.Duracion = duracion;
         }

        /*public DateTime calculoFechaFin()
        {
            if (this.Etapas == null)
                throw new ExcepcionDeProyectos("No hay etapas");

            //Recorre todas las etapas y guarda y devuelve la fecha mas lejana
            
            DateTime etapaUltima = DateTime.Today;

            foreach(var etapas in Etapas){
                etapas.calcFechaFin();

                if (DateTime.Compare(etapaUltima, etapas.FechaFin) > 0) {
                    etapaUltima = etapas.FechaFin;
                    

                }
                FechaFin = etapaUltima;

            }
            return FechaFin;
        }*/


    }
}

