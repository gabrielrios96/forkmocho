﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;

namespace EscuelaNet.Dominio.Proyectos
{
    public class LineaDeProduccion : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public IList<Proyecto> Proyectos { get; set; }
        private LineaDeProduccion()
        {
            this.Proyectos = new List<Proyecto>();
        }
        public LineaDeProduccion(string Nombre) : this()
        {
            this.Nombre = Nombre;
        }

        public void pushProyecto(string nombre, string descripcion, string nombreResponsable, long telefonoResponsable,string emailResponsable)
        {
            if (this.Proyectos == null)
                this.Proyectos = new List<Proyecto>();
            this.Proyectos.Add(new Proyecto(nombre, descripcion, nombreResponsable, telefonoResponsable, emailResponsable));
        }
        public void Editar(string nombre)
        {
            if (nombre == null)
            {
                throw new System.ArgumentNullException(nameof(nombre));
            }
            this.Nombre = nombre;
        }
    }
}