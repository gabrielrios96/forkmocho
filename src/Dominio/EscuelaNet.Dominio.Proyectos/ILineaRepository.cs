﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public interface ILineaRepository : IRepository<LineaDeProduccion>
    {
        LineaDeProduccion Add(LineaDeProduccion linea);
        void Update(LineaDeProduccion linea);
        void Delete(LineaDeProduccion linea);
        //void DeleteProyecto(Proyecto proyecto);
        LineaDeProduccion GetLinea(int id);
        //Proyecto GetProyecto(int id);

        List<LineaDeProduccion> ListLinea();
    }
}
