﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class ExcepcionDeProyectos : Exception
    {
        public ExcepcionDeProyectos(string message) : base(message)
        {
        }
    }
}
