﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Skills : Entity, IAggregateRoot
    {
        public string Descripcion { get; set; }
        public Experiencia Grados { get; set; }

        public Skills(string nombre, Experiencia grado)
        {
            this.Descripcion = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Grados = grado;
        }
        private Skills() { }
    }
}
