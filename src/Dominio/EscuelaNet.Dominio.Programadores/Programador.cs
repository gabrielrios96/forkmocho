﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
 
    public class Programador : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Legajo { get; set; }
        public string Dni { get; set; }
        public string Rol { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EstadoDeDisponibilidad Disponibilidad { get; set; }
        public IList<Skills> Skills { get; set; }
        public int Equipo_ID { get; set; }
        public Equipo Equipo { get; set; }

        public Programador(string nombre, string apellido, string legajo, string dni, string rol, DateTime fecnac)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Legajo = legajo;
            this.Dni = dni;
            this.Rol = rol;
            this.FechaNacimiento = fecnac;
            this.Disponibilidad = EstadoDeDisponibilidad.PartTime;
        }

        private Programador()
        {
            this.Disponibilidad = EstadoDeDisponibilidad.PartTime;
        }

        public void PushConocimiento(Skills skills)
        {
            if (this.Skills == null)
            {
                this.Skills = new List<Skills>();
            }
            this.Skills.Add(skills);
        }
        public void RemoveSkill(Skills skill)
        {
            if (this.Skills != null)
            {
                if (this.Skills.Contains(skill))
                {
                    this.Skills.Remove(skill);
                }
            }
            else
            {
                throw new ExcepcionDeEquipo("No existe el conocimiento para eliminar");
            }
        }

        public void CambiarDisponibilidad(EstadoDeDisponibilidad disponibilidad)
        {

            if (this.Disponibilidad == EstadoDeDisponibilidad.PartTime)
            {
                this.Disponibilidad = disponibilidad;
            }
            else if (this.Disponibilidad == EstadoDeDisponibilidad.FullTime && disponibilidad != EstadoDeDisponibilidad.PartTime)
            {
                this.Disponibilidad = disponibilidad;
            }            
        }

        public string ConsultarDisponibilidad()
        {
            return this.Disponibilidad.ToString();
        }

        public void EditProgramadores(Programador p, string nombre, string apellido, string legajo, string dni, string rol, DateTime fecnac)
        {
            p.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre)); 
            p.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(apellido));
            p.Legajo = legajo ?? throw new System.ArgumentNullException(nameof(legajo));
            p.Dni = dni ?? throw new System.ArgumentNullException(nameof(dni));
            p.Rol = rol ?? throw new System.ArgumentNullException(nameof(rol));
            p.FechaNacimiento = fecnac;
        }

    }
}
