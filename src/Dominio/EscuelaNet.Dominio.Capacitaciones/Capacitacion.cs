﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Capacitacion : Entity, IAggregateRoot
    {
        public IList<Instructor> Instructores { get; set; }
        public Lugar Lugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        private int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public IList<Alumno> Alumnos { get; set; }
        public IList<Tema> Temas { get; set; }

        public void SetDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Error La Duracion de la Capacitacion no puede ser 0");
            Duracion = duracion;
        }
        public int GetDuracion()
        {
            return Duracion;
        }

        public void PushAlumno(Alumno Alumno)
        {
            if (this.Alumnos == null)
            {
                this.Alumnos = new List<Alumno>();
            }
            this.Alumnos.Add(new Alumno(Alumno.Nombre, Alumno.Apellido, Alumno.Dni,Alumno.FechaNacimiento));
        }

        public void AddInstructor(Instructor instructor)
        {
            if (this.Instructores == null)
            {
                this.Instructores = new List<Instructor>();
            }

            this.Instructores.Add(instructor);
                        
        }

        public void AddTema(Tema tema)
        {
            if (this.Temas == null)
            {
                this.Temas = new List<Tema>();
            }

            this.Temas.Add(new Tema(tema.Nombre,tema.Nivel));
        }
    }
}
