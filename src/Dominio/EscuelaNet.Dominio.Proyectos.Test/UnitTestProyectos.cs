﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Proyectos.Test
{
    [TestClass]
    public class UnitTestProyectos
    {
        [TestMethod]
        public void PROBAR_CREAR_UN_PROYECTO()
        {
            var proyecto = new Proyecto("Alejo", "Programa prueba", "Mario de la Barra", 3815056723, "alecjoc85@gmail.com");
            Assert.AreEqual(EstadoDeProyecto.Diseno.ToString(), proyecto.ObtenerEstado());
        }
        [TestMethod]
        public void PROBAR_AGREGAR_ETAPAS()
        {
            var proyecto = new Proyecto("Alejo", "Programa prueba", "Mario de la Barra", 3815056723, "alecjoc85@gmail.com");
            proyecto.PushEtapa("Prueba",10);
            proyecto.EditarDuracionDeEtapa(0,10);
            Assert.AreEqual("Prueba", proyecto.Etapas[0].Nombre);
            Assert.AreEqual(10, proyecto.Etapas[0].Duracion);
            Assert.AreEqual(10, proyecto.Duracion);


        }

        [TestMethod]
        public void PROBAR_INICIAR_UN_PROYECTO()
        {
            var proyecto = new Proyecto("Alejo", "Programa prueba", "Mario de la Barra", 3815056723, "alecjoc85@gmail.com");
            proyecto.CambiarEstado(EstadoDeProyecto.Iniciado);
            Assert.AreEqual(EstadoDeProyecto.Iniciado.ToString(), proyecto.ObtenerEstado());
            proyecto.CambiarEstado(EstadoDeProyecto.Diseno);
            Assert.AreEqual(EstadoDeProyecto.Iniciado.ToString(), proyecto.ObtenerEstado());
        }

        [TestMethod]
        public void PROBAR_CREAR_TECNOLOGIA()
        {
            Action action1 = () =>
            {
                var tecnologia = new Tecnologias(null);
            };
            Assert.ThrowsException<ArgumentNullException>(action1);
        }

        [TestMethod]
        public void PROBAR_PUSH_TECNOLOGIA()
        {
            var proyecto = new Proyecto("Alejo", "Programa prueba", "Mario de la Barra", 3815056723, "alecjoc85@gmail.com");
            proyecto.pushTecnologia("C#");
            Assert.AreEqual("C#", proyecto.Tecnologias[0].nombre);
        }

        }
        [TestMethod]
        public void PROBAR_INGRESAR_PRECIO_NEGATIVO()
        {
            var precio = new Precio();
            Assert.AreEqual(false, precio.ValidarValorNominal(-1));
        }
    }
}
