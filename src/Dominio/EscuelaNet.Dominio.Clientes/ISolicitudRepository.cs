﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public interface ISolicitudRepository : IRepository<Solicitud>
    {
        Solicitud Add(Solicitud solicitud);
        void Update(Solicitud solicitud);
        void Delete(Solicitud solicitud);       
        Solicitud GetSolicitud(int id);
        List<Solicitud> ListSolicitud();
        void addUnidad(Solicitud solicitud, UnidadDeNegocio unidad);
        void removeUnidad(Solicitud solicitud, UnidadDeNegocio unidad);
    }
}
